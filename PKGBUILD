# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Contributor: Gerd Röthig (DAC24)
# Archlinux credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>
# Contributor: Alonso Rodriguez <alonsorodi20 (at) gmail (dot) com>
# Contributor: Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor: Felix Yan <felixonmars@archlinux.org>
# Contributor: loqs
# Contributor: Dede Dindin Qudsy <xtrymind+gmail+com>
# Contributor: Ike Devolder <ike.devolder+gmail+com>

_linuxprefix=linux516-rt
_extramodules=extramodules-5.16-rt-MANJARO
# don't edit here
pkgver=390.147
_nver=390
# edit here for new version
_sver=147
# edit here for new build
pkgrel=5
pkgname=$_linuxprefix-nvidia-${_nver}xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=($CARCH)
url="http://www.nvidia.com/"
depends=("$_linuxprefix" "nvidia-390xx-utils=${_pkgver}")
makedepends=("$_linuxprefix-headers")
groups=("$_linuxprefix-extramodules")
provides=("nvidia=$pkgver")
conflicts=("$_linuxprefix-nvidia" "$_linuxprefix-nvidia-470xx")
license=('custom')
install=nvidia.install
options=(!strip)
_durl="http://us.download.nvidia.com/XFree86/Linux-x86"
source=("${_durl}_64/${_pkgver}/NVIDIA-Linux-${CARCH}-${_pkgver}-no-compat32.run"
        "kernel-5.16.patch")
sha256sums=('3fc4b5a7c64326cea79156fc31e8160a89621219df09a4cd268844c3e318accc'
            'c5120d5640895a3a80fd2315e19c763f956a1822ae2f47854e431c3e7b719e56')

_pkg="NVIDIA-Linux-${CARCH}-${_pkgver}-no-compat32"

pkgver() {
  printf '%s' "${_pkgver}"
}

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"
    patch -p1 -i "$srcdir"/kernel-5.16.patch
    export IGNORE_PREEMPT_RT_PRESENCE=1
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    echo "Kernel Version detected:"
    echo "${_kernver}"
    cd "${_pkg}"/kernel
    make SYSSRC=/usr/lib/modules/"${_kernver}"/build module
}

package() {
    install -D -m644 "${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    install -D -m644 "${_pkg}/kernel/nvidia-modeset.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-modeset.ko"
    install -D -m644 "${_pkg}/kernel/nvidia-drm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-drm.ko"
    install -D -m644 "${_pkg}/kernel/nvidia-uvm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-uvm.ko"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/"*.ko
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${srcdir}/../nvidia.install"
    install -Dm644 "${srcdir}/${_pkg}/LICENSE" "${pkgdir}/usr/share/licenses/nvidia/${pkgname}/LICENSE"
}
